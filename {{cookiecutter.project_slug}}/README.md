# {{cookiecutter.project_name}}

{{cookiecutter.project_summary}}

[![](https://images.microbadger.com/badges/version/mswinson/{{cookiecutter.project_slug}}.svg)](https://microbadger.com/images/mswinson/{{cookiecutter.project_slug}} "Get your own version badge on microbadger.com")
[![](https://images.microbadger.com/badges/image/mswinson/{{cookiecutter.project_slug}}.svg)](https://microbadger.com/images/mswinson/{{cookiecutter.project_slug}} "Get your own image badge on microbadger.com")
